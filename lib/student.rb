class Student

# Student#initialize should take a first and last name.
# Student#name should return the concatenation of the student's first and last name.
# Student#courses should return a list of the Courses in which the student is enrolled.
# Student#enroll should take a Course object, add it to the student's list of courses, and update the Course's list of enrolled students.
# enroll should ignore attempts to re-enroll a student.
# Student#course_load should return a hash of departments to # of credits the student is taking in that department.

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @course_list = []
    @enroll_handled = false
  end

  def first_name
    @first_name
  end

  def last_name
    @last_name
  end

  def courses
    @course_list
  end

  def name
    @full_name = "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    return if @course_list.include?(course)

    @course_list.each do |el|
      if course.conflicts_with?(el)
        raise "You messed up!"
      end
    end

    course.students.push(self)
    @course_list.push(course)
  end

  def course_load
    hash = Hash.new(0)
    @course_list.each do |course|
      hash[course.department] += course.credits
    end

    hash
  end

end
